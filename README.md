**Digitat14 iOS Coding Task**

## Summary
Write a type ahead (search view with results – Search and Listing screen) against the Seat Geek API. The type ahead should update a list of results as the search query changes. Results can be tapped to view them on a details screen.

---

## Tools/Language version required

1. Xcode 12.5 
2. Swift 5.0
3. iOS 14.0 

---

## Application build process 

1. Checkout repo 
2. Goto assignmenttask/Assignment using terminal
3. pod install 
4. Open .xcworkspace using Xcode12.5 
5. Run app 

## Task completed 

1. Search list screen with search functionality 
2. Event details screen where user can mark event favorite or unfavorite just tap on heart button Icon.
3. Used dependency manager i.e cocoapods 
4. Added **unit test** for Searchlist, EventDetails and other functionality.
5. Local data storage used - **UserDefaults** 
6. Architecture used - **Clean Swift**
7. Secure data storage i.e Seat Geek account API key - used **Secure configuration**


