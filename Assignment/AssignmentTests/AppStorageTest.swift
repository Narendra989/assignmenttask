//
//  UtilsTest.swift
//  AssignmentTests
//
//  Created by Narendra Satpute on 15/10/21.
//

import XCTest
@testable import Assignment

class AppStorageTest: XCTestCase {
    
    var appStorage: AppStorage?
    
    override func setUp() {
        appStorage = AppStorage.shared
    }
    
    override func tearDown() {
        appStorage = nil
    }
    
    func test_AddtoFavorite() {
        appStorage?.addToFavoriteEvent(key: 11, value: true)
        let value = appStorage?.retrieveFavoriteEvent(using: 11)
        XCTAssertTrue(value!, "Value should be true")
    }
    
    func test_RemoveFavoriteEvent() {
        appStorage?.removeFavoriteEvent(key: 11)
        let value = appStorage?.retrieveFavoriteEvent(using: 11)
        XCTAssertFalse(value!, "Value should be false")
    }
    
    func test_RemoveAllFavoriteEvent() {
        appStorage?.removeAllFavoriteEvents()
        let value = appStorage?.retrieveFavoriteEvent(using: 11)
        XCTAssertFalse(value!, "Value should be false")
    }
}
