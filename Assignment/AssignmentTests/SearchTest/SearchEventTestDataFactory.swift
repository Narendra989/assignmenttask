//
//  SearchEventTestDataFactory.swift
//  AssignmentTests
//
//  Created by Narendra Satpute on 15/10/21.
//

@testable import Assignment
import Foundation

class SearchEventTestDataFactory {
    
    class func mockSearchResult(searchStr: String) -> SearchResult? {
        if searchStr == "Texas" {
            var eventList = [Event]()
            let venue = Venue(display_location: "test_display_location", name_v2: "test_name_v2")
            let performer = Performers(name: "test_name", image: "test_image", images: DetailImage(huge: "test_huge"), primary: true)
            
            eventList.append(Event(id: 100, title: "test_Title", short_title: "test_short_title", datetime_local: "2022-02-26T03:30:00", url: "test_url", venue: venue, performers: [performer]))
            return SearchResult(events: eventList)
        } else {
            return nil
        }
    }
    
}
