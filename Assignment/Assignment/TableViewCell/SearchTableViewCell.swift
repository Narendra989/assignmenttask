//
//  SearchTableViewCell.swift
//  Assignment
//
//  Created by Narendra Satpute on 14/10/21.
//

import UIKit
import AlamofireImage

class SearchTableViewCell: UITableViewCell {

    @IBOutlet weak var eventImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var favoriteButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        eventImage.layer.cornerRadius = 10.0
        eventImage.layer.masksToBounds = true
        self.selectionStyle = .none
    }

    // MARK: - Class methods
    class func identifier() -> String {
        return AppConstant.searchCellIdentifier
    }
    
    class func nib() -> UINib {
        let nib = UINib(nibName: AppConstant.searchCellIdentifier, bundle: Bundle.main)
        return nib
    }
    
    func configure(event: Event) {
        titleLabel.text = event.title
        locationLabel.text = event.venue?.display_location
        dateLabel.text = event.datetime_local.fomattedDate()
        
        let isFavoriteEvent = AppStorage.shared.retrieveFavoriteEvent(using: event.id)
        if isFavoriteEvent {
            favoriteButton.tintColor = .systemPink
            favoriteButton.isHidden = false
        }  else {
            favoriteButton.isHidden = true
        }
        
        if let performers = event.performers {
            let performerList = performers.filter { $0.primary != nil && $0.primary! }
            if let image = performerList.first?.image {
                eventImage.af.setImage(withURL: URL(string: image)!)
            }
        }
    }
    
}
