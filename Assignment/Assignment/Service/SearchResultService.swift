//
//  SearchResultService.swift
//  Assignment
//
//  Created by Narendra Satpute on 14/10/21.
//

import Foundation
import Alamofire

final class  SearchResultService {
    
    static let shared = SearchResultService()
    let serialQueue = DispatchQueue(label: "SearchResultServiceSerialQueue")
    var clientId = ""
    
    private init() {}
    
    
    func searchEvent(using searchTerm: String, completion: @escaping ((_ searchResult: SearchResult?, _ error: Error?) -> Void)) {
        serialQueue.async {
            
            if let key = Bundle.main.object(forInfoDictionaryKey: "CLIENT_ID") {
                self.clientId = key as! String
                
            }
            let param = ["client_id" : self.clientId, "q" : searchTerm]
            let request = AF.request(AppConstant.baseURL, method: .get, parameters: param, encoding: URLEncoding.queryString)
            
            request.validate(statusCode: 200..<300).responseDecodable(of: SearchResult.self) { (response) in
                
                switch response.result {
                case .success(let result):
                    DispatchQueue.main.async {
                        completion(result, nil)
                    }
                case .failure(let error):
                    DispatchQueue.main.async {
                        completion(nil, error)
                    }
                }
//                guard let results = response.value else {
//                    return completion(nil)
//                }
//
//                DispatchQueue.main.async {
//                    completion(results)
//                }
            }
        }
    }
}


