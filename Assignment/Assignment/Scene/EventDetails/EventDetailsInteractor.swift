//
//  EventDetailsInteractor.swift
//  Assignment
//
//  Created by Narendra Satpute on 14/10/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol EventDetailsBusinessLogic
{
  func fetchEventDetails()
}

class EventDetailsInteractor: EventDetailsBusinessLogic {
    
  var presenter: EventDetailsPresentationLogic?
  var selectedEvent: Event?
  
  // MARK: Do something
  
    func fetchEventDetails() {
        let response = EventDetails.Result.Response(event: selectedEvent)
        presenter?.presentEventDetails(response: response)
    }
}
