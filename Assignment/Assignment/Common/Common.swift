//
//  Common.swift
//  Assignment
//
//  Created by Narendra Satpute on 14/10/21.
//

import Foundation


class AppConstant {
    static let baseURL = "https://api.seatgeek.com/2/events"
    static let searchCellIdentifier = "SearchTableViewCell"
    static let cellHeight = 80.0
    static let eventDetailsViewController = "EventDetailsViewController"
}
