//
//  AppStorage.swift
//  Assignment
//
//  Created by Narendra Satpute on 15/10/21.
//

import Foundation

class AppStorage {
    
    static let shared = AppStorage()
    
    private init() {}
    
    func addToFavoriteEvent(key: Int, value: Bool) {
        UserDefaults.standard.setValue(value, forKey: "\(key)")
        UserDefaults.standard.synchronize()
    }
    
    func removeFavoriteEvent(key: Int) {
        UserDefaults.standard.removeObject(forKey: "\(key)")
        UserDefaults.standard.synchronize()
    }
    
    func retrieveFavoriteEvent(using key: Int) -> Bool {
        let isFavorite = UserDefaults.standard.bool(forKey: "\(key)")
        return isFavorite
    }
    
    func removeAllFavoriteEvents() {
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
    }
}
