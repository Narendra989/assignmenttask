//
//  Utils.swift
//  Assignment
//
//  Created by Narendra Satpute on 15/10/21.
//

import Foundation

extension String {
    func fomattedDate() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        if let date = dateFormatter.date(from: self) {
            dateFormatter.dateFormat = "EEE, dd MMM YYYY HH:mm a"
            return dateFormatter.string(from: date)
        } else {
            return nil
        }
    }
}
